USE petsupplies;

CREATE TABLE `products` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(300) NOT NULL,
  `price` DECIMAL(8,2) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `users` (
  `email` varchar(100) NOT NULL,
  `password` char(64) NOT NULL,
  `full_name` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `postal_code` char(7) NOT NULL,
  `residence` varchar(100) NOT NULL,
  
  PRIMARY KEY (`email`)
);

CREATE TABLE `clients` (
  `client_id` char(36) NOT NULL,
  `client_secret` varchar(100),
  `grant_type` varchar(26) NOT NULL,
  
  PRIMARY KEY (`client_id`)
);

CREATE TABLE `access_tokens` (
	`email` varchar(100) NOT NULL,
    `client_id` char(36) NOT NULL,
    `access_token` char(64) NOT NULL,
    `creation_date` DATETIME	NOT NULL,
    
    PRIMARY KEY (`access_token`),
    FOREIGN KEY (`email`) REFERENCES users(`email`),
    FOREIGN KEY (`client_id`) REFERENCES clients(`client_id`)
);
